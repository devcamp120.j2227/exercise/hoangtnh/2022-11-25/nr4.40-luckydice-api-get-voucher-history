//khai báo thư viện mongoose
const mongoose = require("mongoose");

//khai báo class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khai báo dice history schema
const diceHistorySchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    },
    user: {
        type: String, 
        ref: "Users"
    },
	dice: {
        type:Number, 
        required: true
    },
    createdAt: {
        type: Date, 
        default: Date.now()
    },
    updatedAt: {
        type: Date, 
        default: Date.now()
    }
});

module.exports = mongoose.model("diceHistory", diceHistorySchema);