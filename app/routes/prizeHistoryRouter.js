const express = require("express");
const router = express.Router();
const prizeHisController = require("../controllers/prizeHistoryController");

router.post("/prizeHistories", prizeHisController.createPrizeHistory);
router.get("/prizeHistories", prizeHisController.getAllPrizeHistory);
router.get("/prizeHistories/:prizeHisId", prizeHisController.getPrizeHistoryById);
router.put("/prizeHistories/:prizeHisId", prizeHisController.updatePrizeHistoryById);
router.delete("/prizeHistories/:prizeHisId", prizeHisController.deletePrizeHistoryById);

router.post("/devcamp-lucky-dice/prize-history", prizeHisController.createPrizeHistoryForUser);
router.get("/devcamp-lucky-dice/prize-history", prizeHisController.getPrizeHistoryByUsername);
module.exports = router;