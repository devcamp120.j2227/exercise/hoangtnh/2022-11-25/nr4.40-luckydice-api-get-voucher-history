//import thư viên mongoose
const mongoose = require("mongoose");
//import voucher history model 
const voucherHisModel = require("../models/voucherHistoryModel");
const userModel = require("../models/userModel");
const voucherModel = require("../models/voucherModel")
//function create voucher history
const createVoucherHistory = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    //B3: gọi model tạo dữ liệu
    const newVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        voucher: mongoose.Types.ObjectId(),
        createAt: body.createAt,
        updateAt: body.updatedAt
    }
   
    voucherHisModel.create(newVoucherHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new voucher history successfully",
            data: data
        })
    })
}
//function get all voucher histories
const getAllVoucherHistory = (request, response) => {
    /*voucherHisModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher histories successfully",
            data: data
        })
    })*/
    const userId = request.query.user;
    const condition = {};
    if(userId){
        condition.user = userId
    }
    console.log(condition);
    voucherHisModel.find(condition)
        .exec((error,data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all voucher histories successfully",
                data: data
            })
        })
}
//function get voucher history by id
const getVoucherHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherHisId = request.params.voucherHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHisId)){
        return response.status(400).json({
            status:"Bad Request",
            message: "Voucher history id không hợp lệ"
        })
    }
    //B3: gọi model chưa id prize 
    voucherHisModel.findById(voucherHisId,(error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get voucher history by id ${voucherHisId} successfully`,
            data: data
        })
    })
}
//function update voucher history by id
const updateVoucherHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu'
    const voucherHisId = request.params.voucherHisId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHisId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Voucher History Id không đúng"
        })
    }
    //B3: gọi model chưa id để tìm và update dữ liệu
    const updateVoucherHistory = {};
    if(body.user !== undefined){
        updateVoucherHistory.user = mongoose.Types.ObjectId();
    }
    if(body.voucher !== undefined){
        updateVoucherHistory.prize = mongoose.Types.ObjectId();
    }
    voucherHisModel.findByIdAndUpdate(voucherHisId, updateVoucherHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Update Voucher History successfully",
            data: data
        })
    })
}
//function delete voucher history by id
const deleteVoucherHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherHisId = request.params.voucherHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHisId)){
        return response.status(400).json({
            status:" Bad request",
            message:"Voucher history id không đúng"
        })
    }
    //B3: gọi model chưa id cần xóa
    voucherHisModel.findByIdAndRemove(voucherHisId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`delete Voucher History had id ${voucherHisId} successfully`
        })
    })
}
//function create voucher history for user
const createVoucherHistoryForUser = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    const username = request.query.username;
    //Get random voucher 
    voucherModel.count().exec((err,count) =>{
        //get random entry
        var random = Math.floor(Math.random()*count)
        voucherModel.findOne()
            .skip(random)
            .exec((err, result)=>{
                if(err){
                    return response.status(400).json({
                        status:"Bad request",
                        message: err.message
                    })
                }
                
                const newVoucherHistory = {
                    _id: mongoose.Types.ObjectId(),
                    username: username,
                    voucher: result.code,
                    createAt: body.createAt,
                    updateAt: body.updatedAt
                }
                if(username){
                    voucherHisModel.create(newVoucherHistory, (error, data)=>{
                        if(error){
                            return response.status(500).json({
                                status:"Internal server error",
                                message: error.message
                            })
                        }
                        userModel.findOneAndUpdate({username} , {
                            $push: {
                                voucherhistory: data.voucher
                            }
                        },(err,updateUser) =>{
                            if(err){
                                return response.status(500).json({
                                    status: "Internal server error",
                                    message: err.message
                                })
                            }
                            return response.status(201).json({
                                status:"Create voucher history successfully",
                                data: data
                            })
                        })
                    })
                }
                //nếu không nhập username vào thì tự động tạo mới voucher history
                else{
                    const newVoucherHistory = {
                        _id: mongoose.Types.ObjectId(),
                        username: mongoose.Types.ObjectId(),
                        voucher: result.code,
                        createAt: body.createAt,
                        updateAt: body.updatedAt
                    }
                voucherHisModel.create(newVoucherHistory, (error, data)=>{
                    if(error){
                        return response.status(500).json({
                            status:"Internal server error",
                            message: error.message
                        })
                    }
                    return response.status(201).json({
                        status:"Create voucher history successfully",
                        data: data
                    })
                })
                }
            })
    })
}
//function get voucher history of user
const getVoucherHistoryByUsername = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const username = request.query.username;
    //nếu được truyền vào query user name
    if(username){
        userModel.findOne ({username} ,(errorFindUser, userExist) => {
            if(errorFindUser){
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errorFindUser.message
                })
            } else{
                if(!userExist){ 
                    return response.status(400).json({
                        status:" User name is not valid",
                        data: []
                    })
                } else {
                    userModel.findOne({username} ,(error, data) =>{
                        if(error){
                            return response.status(500).json({
                                status:" Internal server error",
                                message: error.message
                            })
                        }
                        return response.status(200).json({
                            status:`Get Voucher history of user ${username} successfully`,
                            voucherhistory: data.voucherhistory
                        })
                    })
                }
            }
        })
    }
    //nếu không truyền vào query username
    if(username == undefined){  
        voucherHisModel.find((error,data)=>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get all vouchers history successfully",
                data: data
            })
        });
    }
    console.log(username)
}
module.exports ={
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById,

    createVoucherHistoryForUser,
    getVoucherHistoryByUsername
}