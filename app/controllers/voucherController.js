//import thư viện mongoose
const mongoose = require("mongoose");
//import voucher model
const voucherModel = require("../models/voucherModel");

//function create voucher
const createVoucher = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    if(!body.code){
        return response.status(400).json({
            status: "Bad Request",
            message:"Voucher code không hợp lệ"
        })
    }
    if(isNaN(body.discount) || body.discount < 0){
        return response.status(400).json({
            status: "Bad Request",
            message:"Voucher discount không hợp lệ"
        })
    }
    //B3: gọi model tạo dữ liệu
    const newVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note,
        createAt: body.createAt,
        updateAt: body.updatedAt
    }
    if(body.code !== undefined){
        newVoucher.code = body.code
    }
    if(body.discount !== undefined){
        newVoucher.discount = body.discount
    }
    if(body.note !== undefined){
        newVoucher.note = body.note
    }
    voucherModel.create(newVoucher, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new voucher successfully",
            data: data
        })
    })
}
//function get all vouchers
const getAllVouchers = (request, response) => {
    voucherModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all vouchers successfully",
            data: data
        })
    })
}

//function get voucher by id
const getVoucherById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return response.status(400).json({
            status:"Bad Request",
            message: "voucher id không hợp lệ"
        })
    }
    //B3: gọi model chứa id voucher 
    voucherModel.findById(voucherId, (error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get voucher by id ${voucherId} successfully`,
            data: data
        })
    })
}
//function update voucher by id
const updateVoucherById = (request, response) => {
    //B1: chuẩn bị dữ liệu'
    const voucherId = request.params.voucherId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Voucher Id không đúng"
        })
    }
    if(!body.code){
        return response.status(400).json({
            status: "Bad Request",
            message:"Voucher code không hợp lệ"
        })
    }
    if(isNaN(body.discount) || body.discount < 0){
        return response.status(400).json({
            status: "Bad Request",
            message:"Voucher discount không hợp lệ"
        })
    }
    
    //B3: gọi model chưa id để tìm và update dữ liệu
    const updateVoucher = {};
    if(body.code !== undefined){
        updateVoucher.code = body.code
    }
    if(body.discount !== undefined){
        updateVoucher.discount = body.discount
    }
    if(body.note !== undefined){
        updateVoucher.note = body.note
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Update Voucher successfully",
            data: data
        })
    })
}

//function delete voucher by id
const deleteVoucherById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return response.status(400).json({
            status:" Bad request",
            message:"Voucher id không đúng"
        })
    }
    //B3: gọi voucher model chứa id cần xóa
    voucherModel.findByIdAndRemove(voucherId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`delete voucher had id ${voucherId} successfully`
        })
    })
}
module.exports = {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}