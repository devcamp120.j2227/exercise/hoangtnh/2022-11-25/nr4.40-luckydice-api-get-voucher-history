//import thư viên mongoose
const mongoose = require("mongoose");
//import prize history model 
const prizeHisModel = require("../models/prizeHistoryModel");
const userModel = require("../models/userModel");
const prizeModel = require("../models/prizeModel")
//function create prize history
const createPrizeHistory = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    const username = request.query.username;

    //B2: validate dữ liệu
    //B3: gọi model tạo dữ liệu
    const newPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: mongoose.Types.ObjectId(),
        prize: mongoose.Types.ObjectId(),
        createAt: body.createAt,
        updateAt: body.updatedAt
    }
   
    prizeHisModel.create(newPrizeHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new prize history successfully",
            data: data
        })
    })
}
//function get all prize histories
const getAllPrizeHistory = (request, response) => {
    /*prizeHisModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all prize histories successfully",
            data: data
        })
    })*/
    let userId = request.query.user;
    let condition = {};
    if(userId){
        condition.user = userId;
    }
    console.log(condition);
    prizeHisModel.find(condition)
        .exec((error, data) =>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get All prize histories successfully",
                data: data
            })
        })
}

//function get prize history by id
const getPrizeHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeHisId = request.params.prizeHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHisId)){
        return response.status(400).json({
            status:"Bad Request",
            message: "prize history id không hợp lệ"
        })
    }
    //B3: gọi model chưa id prize 
    prizeHisModel.findById(prizeHisId,(error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get prize history by id ${prizeHisId} successfully`,
            data: data
        })
    })
}

//function update prize history by id
const updatePrizeHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu'
    const prizeHisId = request.params.prizeHisId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHisId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Prize History Id không đúng"
        })
    }
    //B3: gọi model chưa id để tìm và update dữ liệu
    const updatePrizeHistory = {};
    if(body.user !== undefined){
        updatePrizeHistory.user = mongoose.Types.ObjectId();
    }
    if(body.prize !== undefined){
        updatePrizeHistory.prize = mongoose.Types.ObjectId();
    }
    prizeHisModel.findByIdAndUpdate(prizeHisId, updatePrizeHistory, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Update Prize History successfully",
            data: data
        })
    })
}

//function delete prize history by id
const deletePrizeHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeHisId = request.params.prizeHisId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHisId)){
        return response.status(400).json({
            status:" Bad request",
            message:"Prize history id không đúng"
        })
    }
    //B3: gọi prize model chưa id cần xóa
    prizeHisModel.findByIdAndRemove(prizeHisId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`delete Prize History had id ${prizeHisId} successfully`
        })
    })
}
// create prize for user
//function create prize history
const createPrizeHistoryForUser = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    const username = request.query.username;
    //Get random prize 
    prizeModel.count().exec((err,count) =>{
        //get random entry
        var random = Math.floor(Math.random()*count)
        prizeModel.findOne()
            .skip(random)
            .exec((err, result)=>{
                if(err){
                    return response.status(400).json({
                        status:"Bad request",
                        message: err.message
                    })
                }
                const newPrizeHistory = {
                    _id: mongoose.Types.ObjectId(),
                    username: username,
                    prize: result.name,
                    createAt: body.createAt,
                    updateAt: body.updatedAt
                }
                if(username){
                    prizeHisModel.create(newPrizeHistory, (error, data)=>{
                        if(error){
                            return response.status(500).json({
                                status:"Internal server error",
                                message: error.message
                            })
                        }
                        userModel.findOneAndUpdate({username} , {
                            $push: {
                                prizehistory: data._id
                            }
                        },(err,updateUser) =>{
                            if(err){
                                return response.status(500).json({
                                    status: "Internal server error",
                                    message: err.message
                                })
                            }
                            return response.status(201).json({
                                status:"Create prize history successfully",
                                data: data
                            })
                        })
                    })
                }
                //nếu không nhập username vào thì tự động tạo mới prize history
                else{
                    const newPrizeHistory = {
                        _id: mongoose.Types.ObjectId(),
                        username: mongoose.Types.ObjectId(),
                        prize: result.name,
                        createAt: body.createAt,
                        updateAt: body.updatedAt
                    }
                prizeHisModel.create(newPrizeHistory, (error, data)=>{
                    if(error){
                        return response.status(500).json({
                            status:"Internal server error",
                            message: error.message
                        })
                    }
                    return response.status(201).json({
                        status:"Create prize history successfully",
                        data: data
                    })
                })
                }
            })
    })
}
//function get prize history of user
const getPrizeHistoryByUsername = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const username = request.query.username;
    //nếu được truyền vào query user name
    if(username){
        userModel.findOne ({username} ,(errorFindUser, userExist) => {
            if(errorFindUser){
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errorFindUser.message
                })
            } else{
                if(!userExist){ 
                    return response.status(400).json({
                        status:" User name is not valid",
                        data: []
                    })
                } else {
                    userModel.findOne({username} ,(error, data) =>{
                        if(error){
                            return response.status(500).json({
                                status:" Internal server error",
                                message: error.message
                            })
                        }
                        return response.status(200).json({
                            status:`Get Prize history of user ${username} successfully`,
                            prizehistory: data.prizehistory
                        })
                    })
                }
            }
        })
    }
    //nếu không truyền vào query username
    if(username == undefined){  
        prizeHisModel.find((error,data)=>{
            if(error){
                return response.status(500).json({
                    status:"Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status:"Get all prizes history successfully",
                data: data
            })
        });
    }
    console.log(username)
}
module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById,

    createPrizeHistoryForUser,
    getPrizeHistoryByUsername
}